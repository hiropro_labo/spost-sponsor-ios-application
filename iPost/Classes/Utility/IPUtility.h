//
//  IPUtility.h
//  iPost
//
//  Created by 藤田　龍 on 2013/09/02.
//  Copyright (c) 2013年 hiropro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GLobalModel.h"

@interface IPUtility : NSObject

+ (NSString *)dateFormatWithJsonTime:(NSString *)strTime format:(NSString *)format;
+ (NSDictionary *)createDateTimeFromUnixTime:(NSString *)linuxTime;
+ (void)telephoneCallWithStringNumber:(NSString *)strNumber;
+ (void)mailToWithStringAddress:(NSString *)address;
+ (UIStoryboard *)getMainStoryBoard;
+ (void)connectionAlertShow;
+ (void)convertAlertShow;
+ (void)mainteAlertShow;
+ (void)stopPublishing;
+ (void)wait:(int)len;
+ (UIImage *)setNavigationBarSetting:(UIViewController *)naviController;
+ (UIImage *)setNavigationItem:(UIViewController *)viewController;
+ (UIColor *)colorWithHexString:(NSString *)hex alpha:(CGFloat)alpha;
@end
