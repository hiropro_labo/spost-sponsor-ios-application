//
//  IPUtility.m
//  iPost
//
//  Created by 藤田　龍 on 2013/09/02.
//  Copyright (c) 2013年 hiropro. All rights reserved.
//

#import "IPUtility.h"

@implementation IPUtility

/*
 * JSONデータが秒数を格納しているので、日付の様式に変換
 */
+ (NSString *)dateFormatWithJsonTime:(NSString *)strTime format:(NSString *)format
{
    NSTimeInterval interval = [strTime doubleValue];
    NSDate *expiresDate = [NSDate dateWithTimeIntervalSince1970:interval];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    // [formatter setDateFormat:@"dd/MM/yyyy HH:mm"];
    [formatter setDateFormat:format];
    NSString *result = [formatter stringFromDate:expiresDate];
    
    return result;
}

+ (NSDictionary *)createDateTimeFromUnixTime:(NSString *)linuxTime
{
    NSTimeInterval interval = [linuxTime doubleValue];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:interval];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *comps;

    NSMutableDictionary *dateDictionary = [[NSMutableDictionary alloc] init];
    
    // 年月日をとりだす
    comps = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit)
                        fromDate:date];
    
    [dateDictionary setObject:[NSString stringWithFormat:@"%d", (int)[comps year]] forKey:@"year"];
    [dateDictionary setObject:[NSString stringWithFormat:@"%d", (int)[comps month]] forKey:@"month"];
    [dateDictionary setObject:[NSString stringWithFormat:@"%d", (int)[comps day]] forKey:@"day"];
    
    // 時分秒をとりだす
    comps = [calendar components:(NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit)
                        fromDate:date];

    //23時間表示を12時間表示に
    int hour = (int)[comps hour];
    if([comps hour] > 12){
        hour = (hour - 12);
    }

    [dateDictionary setObject:[NSString stringWithFormat:@"%d", hour] forKey:@"ampm"];
    [dateDictionary setObject:[NSString stringWithFormat:@"%d", (int)[comps hour]] forKey:@"hour"];
    [dateDictionary setObject:[NSString stringWithFormat:@"%d", (int)[comps minute]] forKey:@"minute"];
    [dateDictionary setObject:[NSString stringWithFormat:@"%d", (int)[comps second]] forKey:@"second"];

    // 週や曜日などをとりだす
    comps = [calendar components:(NSWeekCalendarUnit | NSWeekdayCalendarUnit | NSWeekdayOrdinalCalendarUnit)
                        fromDate:date];
    [dateDictionary setObject:[NSString stringWithFormat:@"%d", (int)[comps week]] forKey:@"week"];
    [dateDictionary setObject:[NSString stringWithFormat:@"%d", (int)[comps weekdayOrdinal]] forKey:@"weekdayOrdinal"];

    NSArray *weekArray = [NSArray arrayWithObjects:@"", @"SUNDAY", @"MONDAY", @"TUESDAY", @"WEDNESDAY", @"THURSDAY", @"FRIDAY", @"SATURDAY", nil];
    [dateDictionary setObject:[weekArray objectAtIndex:[comps weekday]] forKey:@"weekday"];
    
    return [dateDictionary copy];
}

/*
 16進数カラーコードをUIColorに変換
 */
+ (UIColor *)colorWithHexString:(NSString *)hex alpha:(CGFloat)alpha
{
    //先頭に#がついていた場合は#を削除
    if ([hex hasPrefix:@"#"]) {
        hex = [hex substringFromIndex:1];
    }
    
    unsigned int rgb[3];
    for (int i = 0; i < 3; i++) {
        NSString *component = [hex substringWithRange:NSMakeRange(i * 2, 2)];
        NSScanner *scanner = [NSScanner scannerWithString:component];
        [scanner scanHexInt:&rgb[i]];
    }
    return [UIColor colorWithRed:rgb[0]/255.0 green:rgb[1]/255.0 blue:rgb[2]/255.0 alpha:1.0f];
}

+ (UIImage *)setNavigationBarSetting:(UIViewController *)viewController
{
    GLobalModel *globalModel = [GLobalModel alloc];
    NSDictionary *globalData = [globalModel load:DEF_GLOBAL_JSON];
    NSDictionary *barData =    [globalData objectForKey:JSON_KEY_GLOBAL_BARDATA];
    
    // タイトルに貼付けたいイメージ
    if([[globalData objectForKey:JSON_KEY_GLOBAL_APPICON] isEqualToString:@""])
    {
        viewController.navigationItem.title = [globalData objectForKey:JSON_KEY_GLOBAL_APP_NAME];
    }
    else
    {
        NSString *strURL = [NSString stringWithFormat:@"%@%@",
                            URL_IMG_LOGO,
                            [globalData objectForKey:JSON_KEY_GLOBAL_APPICON]];
        NSURL *urlImage = [NSURL URLWithString:strURL];
        NSData *imageData = [[NSData alloc] initWithContentsOfURL:urlImage];
        
        UIImage *image = [UIImage imageWithData:imageData];
    
        if(image){
            // イメージのサイズを調節
            CGSize viewSize = CGSizeMake(image.size.width, image.size.height);
            CGFloat imageHeight = 32;
            CGSize titleImageSize = CGSizeMake(viewSize.width * (imageHeight / viewSize.height), imageHeight);
    
            // UIImageViewをつくる
            UIImageView* titleImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, titleImageSize.width, imageHeight)];
            titleImageView.backgroundColor = [UIColor clearColor];
            
            if (floor(NSFoundationVersionNumber) >= NSFoundationVersionNumber_iOS_6_1) {
                image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
            }
            
            titleImageView.image = image;

            // UIImageViewをのっけるUIViewをつくる
            UIView* bgView = [[UIView alloc] initWithFrame:titleImageView.frame];
            bgView.backgroundColor = [UIColor clearColor];
            [bgView addSubview:titleImageView];
    
            //UINavigationBarのタイトルに設定。selfはViewControllerです。
            viewController.navigationItem.titleView = bgView;
        }
        else{
            viewController.navigationItem.title = [globalData objectForKey:JSON_KEY_GLOBAL_APP_NAME];
        }
    }
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        viewController.navigationController.navigationBar.tintColor = [IPUtility colorWithHexString:[barData objectForKey:JSON_KEY_GLOBAL_BARCOLOR] alpha:1];
    }
    else{
        [UINavigationBar appearance].barTintColor = [IPUtility colorWithHexString:[barData objectForKey:JSON_KEY_GLOBAL_BARCOLOR] alpha:1];
    }
    
    NSString *rightButtonImageStrURL = [NSString stringWithFormat:@"%@%@",
                        URL_IMG_ICON,
                        [barData objectForKey:JSON_KEY_GLOBAL_NAVIMENU]];
    NSURL *rightButtonUrlImage = [NSURL URLWithString:rightButtonImageStrURL];
    NSData *rightButtonImageData = [[NSData alloc] initWithContentsOfURL:rightButtonUrlImage];
    
    UIImage *rightButtonImage = [UIImage imageWithData:rightButtonImageData];
    
    if(!rightButtonImage){
        rightButtonImage = [UIImage imageNamed:@"btn_navigation_menu.png"];
    }
    
    UIGraphicsBeginImageContext(CGSizeMake(20, 21));
    [rightButtonImage drawInRect:CGRectMake(0, 0, 20, 21)];
    rightButtonImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    if (floor(NSFoundationVersionNumber) >= NSFoundationVersionNumber_iOS_6_1) {
        rightButtonImage = [rightButtonImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    }

    return rightButtonImage;
}

+ (UIImage *)setNavigationItem:(UIViewController *)viewController
{
    GLobalModel *globalModel = [GLobalModel alloc];
    NSDictionary *globalData = [globalModel load:DEF_GLOBAL_JSON];
    NSDictionary *barData =    [globalData objectForKey:JSON_KEY_GLOBAL_BARDATA];
    
    NSString *strURL = [NSString stringWithFormat:@"%@%@",
                        URL_IMG_ICON,
                        [barData objectForKey:JSON_KEY_GLOBAL_NAVIBACK]];
    NSURL *urlImage = [NSURL URLWithString:strURL];
    NSData *imageData = [[NSData alloc] initWithContentsOfURL:urlImage];
    
    UIImage *image = [UIImage imageWithData:imageData];
    
    if(!image){
        image = [UIImage imageNamed:@"btn_navigation_back.png"];
    }

    UIGraphicsBeginImageContext(CGSizeMake(12, 21));
    [image drawInRect:CGRectMake(0, 0, 12, 21)];
    image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    if (floor(NSFoundationVersionNumber) >= NSFoundationVersionNumber_iOS_6_1) {
        image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    }
    
    UIBarButtonItem *nativBackBarButtonItem =
    [[UIBarButtonItem alloc] initWithImage:image
                                     style:UIBarButtonItemStylePlain
                                    target:viewController.navigationController
                                    action:@selector(popViewControllerAnimated:)];

    viewController.navigationItem.leftBarButtonItem = nativBackBarButtonItem;
    viewController.navigationItem.leftBarButtonItem.tintColor = [UIColor clearColor];
    
    return image;
}

- (void)back:(UIViewController *)viewController
{
    
    [viewController.navigationController popViewControllerAnimated:YES];
}

+ (void)telephoneCallWithStringNumber:(NSString *)strNumber
{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"tel://%@", strNumber]];
    [self openUrlWithUrl:url];
}


+ (void)mailToWithStringAddress:(NSString *)address
{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"mailto:%@", address]];
    [self openUrlWithUrl:url];
}


+ (void)openUrlWithUrl:(NSURL *)url
{
    TRACE(@"url = %@", url);
    [[UIApplication sharedApplication] openURL:url];
}


+ (UIStoryboard *)getMainStoryBoard
{
    NSString *value = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"UIMainStoryboardFile"];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:value bundle:nil];
    
    return storyboard;
}


+ (void)connectionAlertShow
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message:@"サーバーに接続できませんでした。\nインターネットに接続されているか確認して下さい。"
                                                   delegate:self
                                          cancelButtonTitle:@"閉じる"
                                          otherButtonTitles:nil, nil];
    [alert show];
}


+ (void)convertAlertShow
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message:@"サーバーから情報が取得できておりません。\nインターネットに接続されているか確認の上、アプリケーションを起動し直して下さい。"
                                                   delegate:self
                                          cancelButtonTitle:@"閉じる"
                                          otherButtonTitles:nil, nil];
    [alert show];
}


+ (void)mainteAlertShow
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message:@"現在、サーバーがメンテナンス中のため、サーバーから情報が取得できておりません。\n時間をおいてから再度、アプリケーションを起動し直して下さい。"
                                                   delegate:self
                                          cancelButtonTitle:@"閉じる"
                                          otherButtonTitles:nil, nil];
    [alert show];
}

+ (void)stopPublishing
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message:@"現在アプリの公開を停止しております。\nご利用のお客様にご迷惑をおかけしていることをお詫び申し上げます。"
                                                   delegate:self
                                          cancelButtonTitle:@"閉じる"
                                          otherButtonTitles:nil, nil];
    [alert show];
}

/**
 * 指定したミリ秒待つようにする
 */
+ (void)wait:(int)len
{
    TRACE(@"WAIT...");
    
    int cnt = 0;
    // .3秒待つ
    while (cnt < len) {
        [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.0]];
        [NSThread sleepForTimeInterval:0.1];
        cnt ++;
    }
}

@end
