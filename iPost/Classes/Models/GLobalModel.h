//
//  IPMainteModel.h
//  iPost
//
//  Created by 藤田　龍 on 2013/08/27.
//  Copyright (c) 2013年 hiropro. All rights reserved.
//

/*
 * データ格納内容
 *  _arrayImageInfo  : 画面上部の画像情報をNSDictionary形式で格納
 *  _arrayImages     : 上記の画像データをUIImageで格納
 *  _arrayData       : お店情報をNSDictionaryで格納
 *  app_iconとかapp_image
 */

#import "IPModel.h"
#import "IPHTTPLoader.h"

/*
"app_icon" = "1403065095.png";
"app_name" = "\U30c6\U30b9\U30c8\U30a2\U30d7\U30ea";
copyright = "Copyright \U00a9 2013 HIROPRO, Inc. All Rights Reserved.";
maintenance = "<null>";
"status_bar" =     {
    "btn_back" = "btn_back_1.png";
    "btn_menu" = "btn_menu_1.png";
    color = "#FF0000";
};
*/
#define JSON_KEY_GLOBAL_BARDATA           @"status_bar"
#define JSON_KEY_GLOBAL_MAINTESTATUS      @"maintenance"
#define JSON_KEY_GLOBAL_ALIVE             @"alive"
#define JSON_KEY_GLOBAL_UPDATED           @"updated_at"
#define JSON_KEY_GLOBAL_APPICON           @"app_icon"
#define JSON_KEY_GLOBAL_APP_NAME          @"app_name"
#define JSON_KEY_GLOBAL_BARCOLOR          @"color"
#define JSON_KEY_GLOBAL_NAVIMENU          @"btn_menu"
#define JSON_KEY_GLOBAL_NAVIBACK          @"btn_back"

/*
// JSONインデックスキー
#define + (void)stopPublishing       @"app_icon"
#define JSON_KEY_MAIN_APP_NAME       @"app_name"

#define JSON_KEY_MAINTENANCE_VALUE        @"maintenance"
#define JSON_KEY_MAINTENANCE_SHOPSTATUS   @"shop_status"

#define JSON_KEY_MAINTE_ID                @"id"
#define JSON_KEY_MAINTE_CLIENTID          @"client_id"
#define JSON_KEY_MAINTE_STATUS_CODE       @"status"
#define JSON_KEY_MAINTE_EXPIRED           @"expired_at"
#define JSON_KEY_MAINTE_UPDATED           @"updated_at"
*/

@interface GLobalModel : IPModel

- (void)download:(BOOL)check block:(void (^)())block;
- (BOOL)saveIsOk;
@end
