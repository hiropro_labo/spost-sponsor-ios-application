//
//  IPMainMenuModel.h
//  iPost.mock.v4
//
//  Created by ヒロ企画 on 2014/04/23.
//  Copyright (c) 2014年 北川 義隆. All rights reserved.
//

#import "IPModel.h"
#import "IPHTTPLoader.h"


// JSONインデックスキー
#define JSON_KEY_SLIDEMENU_LIST          @"list_menu"
#define JSON_KEY_SLIDEMENU_CONTROLLER    @"controller"
#define JSON_KEY_SLIDEMENU_NAME          @"name"
#define JSON_KEY_SLIDEMENU_POS           @"position"
#define JSON_KEY_SLIDEMENU_FILENAME      @"icon"
#define JSON_KEY_SLIDEMENU_VIEWTYPE      @"viewtype"
#define JSON_KEY_SLIDEMENU_EMAIL         @"email"
#define JSON_KEY_SLIDEMENU_URL           @"url"


@interface IPMainMenuModel : IPModel

- (void)download:(BOOL)check block:(void (^)())block;
- (BOOL)saveIsOk;
@end
