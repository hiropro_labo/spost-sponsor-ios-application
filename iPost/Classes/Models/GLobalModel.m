//
//  IPGlobalModel.m
//  iPost
//
//  Created by 藤田　龍 on 2013/08/27.
//  Copyright (c) 2013年 hiropro. All rights reserved.
//

#import "GLobalModel.h"

@interface GLobalModel ()

@end


@implementation GLobalModel
{
    BOOL isOk;
}

#pragma mark - connection
/**
 * JSON を サーバーから読み込む
 */
- (void)download:(BOOL)check block:(void (^)())block
{

    [IPHTTPLoader loadDataWithURL:[NSURL URLWithString:URL_API_MAINTE]
            requestParam:nil
                success:^(AFHTTPRequestOperation *operation, id resultJson)
                {
                    TRACE(@"success!");
                    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                    [self save:resultJson operation:operation];
                }
                failure:^(AFHTTPRequestOperation *operation, NSError *error)
                {
                    TRACE(@"App.net Error : %@", [error localizedDescription]);
                    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                }
     ];
    [self wait];
}


/**
 * JSON を NSUserDefaults に保存
 */
- (void)save:(NSDictionary *)json operation:(AFHTTPRequestOperation *)operation
{
    defaults = [NSUserDefaults standardUserDefaults];

    [defaults setObject:operation.responseString forKey:DEF_GLOBAL_JSON];
    [defaults synchronize];
    
    isOk = YES;
}

- (BOOL)saveIsOk
{
    return isOk;
}


@end
