//
//  IPMainMenuModel.m
//  iPost.mock.v4
//
//  Created by ヒロ企画 on 2014/04/23.
//  Copyright (c) 2014年 北川 義隆. All rights reserved.
//

#import "IPMainMenuModel.h"

@interface IPMainMenuModel () {
    NSString *_strRevision;
    NSNumber *_numLimitStart;
    
    BOOL isOk;
}
@end

@implementation IPMainMenuModel

#pragma mark - connection
- (void)download:(BOOL)check block:(void (^)())block
{
    [IPHTTPLoader loadDataWithURL:[NSURL URLWithString:URL_API_SLIDEMENU]
                    requestParam:nil
                    success:^(AFHTTPRequestOperation *operation, id resultJson)
    {
        TRACE(@"success!");
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [self save:resultJson operation:operation];
    }
                        failure:^(AFHTTPRequestOperation *operation, NSError *error)
    {
        TRACE(@"App.net Error : %@", [error localizedDescription]);
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    }];
    
    [self wait];
}

- (BOOL)saveIsOk
{
    return isOk;
}

/**
 * JSON を NSUserDefaults に保存
 */
- (void)save:(id)json operation:(AFHTTPRequestOperation *)operation
{
    defaults = [NSUserDefaults standardUserDefaults];
    
    NSDate *jsonTime = [NSDate date];
    
    TRACE(@"JSON SAVE : %@", jsonTime);
    // JSON 保存
    [defaults setObject:jsonTime forKey:DEF_SLIDEMENU_JSON_TIME];
    [defaults setObject:operation.responseString forKey:DEF_SLIDEMENU_JSON];
    isOk = YES;
}


@end
