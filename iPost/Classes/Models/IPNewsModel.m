//
//  IPNewsModel.m
//  iPost
//
//  Created by 藤田　龍 on 2013/09/01.
//  Copyright (c) 2013年 hiropro. All rights reserved.
//

#import "IPNewsModel.h"


@interface IPNewsModel () {
    NSString *_strRevision;
    NSNumber *_numLimitStart;
    
    BOOL isOk;
}
@end


@implementation IPNewsModel

#pragma mark - connection
- (void)download:(BOOL)check block:(void (^)())block
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    
    NSDictionary *tokenDictionary;
    
    if ([ud objectForKey:DEF_TOKEN]) {
        tokenDictionary = [NSDictionary dictionaryWithObject:[ud objectForKey:DEF_TOKEN] forKey:@"token"];
    }

    [IPHTTPLoader loadDataWithURL:[NSURL URLWithString:URL_API_NEWS]
                     requestParam:tokenDictionary
                          success:^(AFHTTPRequestOperation *operation, id resultJson)
                          {
                              TRACE(@"success!");
                              [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                              [self save:resultJson operation:operation];
                          }
                          failure:^(AFHTTPRequestOperation *operation, NSError *error)
                          {
                              TRACE(@"App.net Error : %@", [error localizedDescription]);
                              [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                          }];
    [self wait];
}

/**
 * JSON を NSUserDefaults に保存
 */
- (void)save:(id)json operation:(AFHTTPRequestOperation *)operation
{
    defaults = [NSUserDefaults standardUserDefaults];
    
    NSDate *jsonTime = [NSDate date];
    
    TRACE(@"JSON SAVE : %@", jsonTime);
    // JSON 保存
    [defaults setObject:jsonTime forKey:DEF_NEWS_JSON_TIME];
    [defaults setObject:operation.responseString forKey:DEF_NEWS_JSON];
    isOk = YES;
}

- (BOOL)saveIsOk
{
    return isOk;
}

- (NSArray *)loadMonthList:(NSInteger)index
{
    NSDictionary *listDictionary = [self load:DEF_NEWS_JSON];
    
    NSArray *keys = [listDictionary allKeys];
    NSArray *monthArray = [listDictionary objectForKey:[keys objectAtIndex:index]];
    
    return monthArray;
}

/**
 * NSUserDefaults から JSON を読み込む
 */
- (NSDictionary *)loadList:(NSString *)parent block:(void (^)())block
{
    NSDictionary *jsonDic = [self load:parent];
    
    //    return [jsonDic objectForKey:JSON_KEY_MENU_LIST];
    return jsonDic;
}

@end
