//
//  SettingWebViewController.m
//  iPost.mock.v3
//
//  Created by 北川 義隆 on 2014/03/22.
//  Copyright (c) 2014年 北川 義隆. All rights reserved.
//

#import "SettingWebViewController.h"

@interface SettingWebViewController () {
    IBOutlet UIWebView *_webView;
    NSURL *_currURL;
}

@end

@implementation SettingWebViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    TRACE(@"");
    
    GLobalModel *globalModel = [GLobalModel alloc];
    NSDictionary *globalData = [globalModel load:DEF_GLOBAL_JSON];
    NSDictionary *barData =    [globalData objectForKey:JSON_KEY_GLOBAL_BARDATA];
    
    NSString *strURL = [NSString stringWithFormat:@"%@%@",
                        URL_IMG_ICON,
                        [barData objectForKey:JSON_KEY_GLOBAL_NAVIBACK]];
    NSURL *urlImage = [NSURL URLWithString:strURL];
    NSData *imageData = [[NSData alloc] initWithContentsOfURL:urlImage];
    
    UIImage *image = [UIImage imageWithData:imageData];
    if(!image){
        image = [UIImage imageNamed:@"btn_navigation_menu.png"];
        [UINavigationBar appearance].tintColor = [UIColor blackColor];
    }
    else{
        //image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    }
    
    UIBarButtonItem *backBarButtonItem =
    [[UIBarButtonItem alloc] initWithImage:image
                                     style:UIBarButtonItemStylePlain
                                    target:self.navigationController
     
                                    action:@selector(popViewControllerAnimated:)];
    
    self.navigationItem.leftBarButtonItem = backBarButtonItem;
    
    //UIScrollViewを取得後、bouncesをNOに変更
    for (id subview in _webView.subviews){
        if([[subview class] isSubclassOfClass: [UIScrollView class]]){
            ((UIScrollView *)subview).bounces = NO;
        }
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    TRACE(@"");
    
    [self loadPageByServerData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - delegate

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    TRACE(@"");
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    TRACE(@"");
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

- (void)webView:(UIWebView*)webView didFailLoadWithError:(NSError*)error
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

#pragma mark - public method

- (void)setURL:(NSURL *)url
{
    TRACE(@"url = %@", url);
    _currURL = url;
    TRACE(@"_currURL = %@", _currURL);
}

- (void)requestPage
{
    TRACE(@"");
    NSURLRequest *req = [NSURLRequest requestWithURL:_currURL];
    [[UIWebView appearance] loadRequest:req];
}

#pragma mark - private method

- (void)loadPageByServerData
{
    [self requestPage];
}

@end
