//
//  NavigationController.m
//  iPost-Mock3
//
//  Created by 北川 義隆 on 2013/12/01.
//  Copyright (c) 2013年 北川 義隆. All rights reserved.
//

#import "NavigationController.h"
#import "GLobalModel.h"

@interface NavigationController ()

@end

@implementation NavigationController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
