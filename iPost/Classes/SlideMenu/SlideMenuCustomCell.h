//
//  SlideMenuCustomCell.h
//  iPost.mock.v4
//
//  Created by ヒロ企画 on 2014/04/24.
//  Copyright (c) 2014年 北川 義隆. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SlideMenuCustomCell : UITableViewCell
@property (readonly, nonatomic) IBOutlet UIImageView *imageView;
@property (readonly, nonatomic) IBOutlet UILabel *textLabel;

@end
