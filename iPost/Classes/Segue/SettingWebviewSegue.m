//
//  SettingWebviewSegue.m
//  iPost.mock.v3
//
//  Created by 北川 義隆 on 2014/03/22.
//  Copyright (c) 2014年 北川 義隆. All rights reserved.
//

#import "SettingWebviewSegue.h"
#import "SettingViewController.h"
#import "WebViewController.h"

@implementation SettingWebviewSegue

- (void)perform
{
    SettingViewController *srcViewController = (SettingViewController *)self.sourceViewController; //遷移元
    WebViewController *destViewController = (WebViewController *)self.destinationViewController; //遷移先
    
    // URLデータ有無チェック
    NSString *strUrl = [srcViewController getUrl];
    TRACE(@"strUrl = %@", strUrl);
    if(strUrl == nil || [strUrl isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                        message:@"情報がありません。"
                                                       delegate:self
                                              cancelButtonTitle:@"閉じる"
                                              otherButtonTitles:nil, nil];
        [alert show];
    } else {
        [destViewController setURL:[NSURL URLWithString:strUrl]];
        destViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        [srcViewController presentViewController:destViewController animated:YES completion:nil];
    }
    
}

@end