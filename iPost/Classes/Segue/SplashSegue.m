//
//  SplashSegue.m
//  iPost-Mock3
//
//  Created by 北川 義隆 on 2013/10/26.
//  Copyright (c) 2013年 北川 義隆. All rights reserved.
//

#import "SplashSegue.h"

#import "ViewController.h"
#import "SlideViewController.h"


@implementation SplashSegue

- (void)perform
{
    //遷移元
    ViewController *srcViewController = (ViewController *)  self.sourceViewController;
    
    //遷移先
    SlideViewController *destViewController = (SlideViewController *) self.destinationViewController;
    
    [srcViewController.navigationController pushViewController:destViewController animated:NO];
}

@end
