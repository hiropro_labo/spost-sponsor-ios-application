//
//  SettingHelpWebviewSeque.m
//  iPost.mock.v3
//
//  Created by 北川 義隆 on 2014/03/22.
//  Copyright (c) 2014年 北川 義隆. All rights reserved.
//

#import "SettingHelpWebviewSeque.h"
#import "SettingViewController.h"
#import "SettingWebViewController.h"

@implementation SettingHelpWebviewSeque

- (void)perform
{
    SettingViewController *srcViewController = (SettingViewController *)self.sourceViewController; //遷移元
    SettingWebViewController *destViewController = (SettingWebViewController *)self.destinationViewController; //遷移先
    
    // URLデータ有無チェック
    NSString *strUrl = [srcViewController getUrl];
    TRACE(@"strUrl = %@", strUrl);
    if(strUrl == nil || [strUrl isEqualToString:@""]) {
    } else {
        [destViewController setURL:[NSURL URLWithString:strUrl]];
        [srcViewController.navigationController pushViewController:destViewController animated:YES];
    }
    
}

@end