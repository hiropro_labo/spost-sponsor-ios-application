//
//  NewsWebSeque.m
//  iPost.mock.v3
//
//  Created by 北川 義隆 on 2014/03/25.
//  Copyright (c) 2014年 北川 義隆. All rights reserved.
//

#import "NewsWebSeque.h"
#import "NewsViewController.h"
#import "WebViewController.h"

@implementation NewsWebSeque

- (void)perform
{
    NewsViewController *srcViewController = (NewsViewController *)  self.sourceViewController; //遷移元
    WebViewController *destViewController = (WebViewController *) self.destinationViewController; //遷移先
    

//        [destViewController setURL:strUrl];
        destViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        [srcViewController presentViewController:destViewController animated:YES completion:nil];
  //  }
}

@end
