//
//  NewsViewController.m
//  sPost.sponsor.mock.v1
//
//  Created by ヒロ企画 on 2014/06/17.
//  Copyright (c) 2014年 北川 義隆. All rights reserved.
//

#import "NewsViewController.h"
#import "NewsCustomCell.h"
#import "IPNewsModel.h"
#import "NewsDetailViewController.h"

#define NEWS_CUSTOMCELL @"NewsCustomCell"

@interface NewsViewController ()
{
    IPNewsModel *_model;
    NSDictionary *newsArraySection;
}
@end

@implementation NewsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    TRACE(@"");
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    //フッターの設定
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 62)];
    UILabel *footerLabel = [[UILabel alloc] initWithFrame:footerView.frame];
    footerLabel.textAlignment = NSTextAlignmentCenter;
    footerLabel.textColor = RGB(146, 146, 146);
    footerLabel.font = [UIFont fontWithName:@"Helvetica" size:10];
    
    /*
    (保留)コピーライトJSONデータで
    footerLabel.text = @"Copyright © 2014 HIROPRO, Inc. Allrights Reserved.";
    [footerView addSubview:footerLabel];
    footerView.backgroundColor = RGB(46, 46, 46);
    self.tableView.tableFooterView = footerView;
*/
    CGRect screenSize = [[UIScreen mainScreen] applicationFrame];

    if(screenSize.size.width == 320.0 && screenSize.size.height == 460)
    {
        CGRect rect = self.pageTitleView.frame;
        rect.origin.y = 64;
        rect.size.height = 44;
        self.pageTitleView.frame = rect;
    }

    //navigationBar設定
    [IPUtility setNavigationBarSetting:self];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self newsDataRefresh];
    
    [self.tableView registerNib:[UINib nibWithNibName:NEWS_CUSTOMCELL bundle:nil] forCellReuseIdentifier:NEWS_CUSTOMCELL];
}

- (void)newsDataRefresh
{
    // News Model の読み込み
    if ( ! _model || _model == nil) {
        _model = [[IPNewsModel alloc] init];
    }
    
    [_model download:YES block:nil];
    
    newsArraySection = [[NSDictionary alloc] init];
    newsArraySection = [_model load:DEF_NEWS_JSON];
    
    [self.tableView reloadData];
}

#pragma mark - Table view data source

/**
 * テーブル全体のセクションの数を返す
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [newsArraySection count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSArray *keys = [newsArraySection allKeys];
    
    return [keys objectAtIndex:section];
    //return @"hoge";
}

//セクションヘッダのカスタム
- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    if ([view isKindOfClass: [UITableViewHeaderFooterView class]]) {
        UITableViewHeaderFooterView* castView = (UITableViewHeaderFooterView*) view;
        UIView* content = castView.contentView;
        UIColor* color = RGB(243, 241, 241); // substitute your color here
        content.backgroundColor = color;
        
        castView.textLabel.textAlignment = NSTextAlignmentCenter;
        [castView.textLabel setTextColor:RGB(47, 166, 92)];
    }
}

/*
//フッター設定
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *footerView =
}
//フッター高さ
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 60;
}
*/

/*セクションヘッダの高さ
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    
}
*/
/**
 * セルの数
 */
- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    NSArray *newsArray = [_model loadMonthList:section];
    return newsArray.count;
}


/**
 * セルの高さ
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}


/**
 * テーブルのセル表示処理
 */
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   // NewsCustomCell *cell = [self.tableView dequeueReusableCellWithIdentifier:NEWS_CUSTOMCELL forIndexPath:indexPath];
    
    NewsCustomCell *cell = [self.tableView dequeueReusableCellWithIdentifier:NEWS_CUSTOMCELL forIndexPath:indexPath];
    
    NSArray *newsArray = [_model loadMonthList:indexPath.section];
    NSDictionary *newsDictionary = [newsArray objectAtIndex:indexPath.row];

    // 画像ダウンロード
    NSString *strURL = [NSString stringWithFormat:@"%@%@",
                        URL_IMG_NEWS,
                        [newsDictionary objectForKey:JSON_KEY_NEWS_FILE]];
    NSURL *urlImage = [NSURL URLWithString:strURL];
    
    //画像がずれる症状のとりあえずの処置
    cell.contentView.frame = CGRectMake(0, 0, 0, 0);
    
    [cell.listImage setImageWithURL:urlImage placeholderImage:[UIImage imageNamed:IMAGE_PALCEHOLDERIMAGE_NEWS2]];
    
    //タイトル
    cell.titleLabel.text = [newsDictionary objectForKey:JSON_KEY_NEWS_TITLE];
    
    NSString *strDate = [newsDictionary objectForKey:JSON_KEY_NEWS_UPDATED];
    NSDictionary *dateDictionary = [IPUtility createDateTimeFromUnixTime:strDate];

    //年月日
    cell.yearLabel.text = [NSString stringWithFormat:@"%@/%@",
                           [dateDictionary objectForKey:@"year"],
                           [dateDictionary objectForKey:@"month"]];
    //日付
    cell.dayLabel.text = [NSString stringWithFormat:@"%02d",
                          [[dateDictionary objectForKey:@"day"] intValue]];
    
    //時間
    NSString *AmPm = @"AM";
    if ([[dateDictionary objectForKey:@"hour"] intValue] >= 12) AmPm = @"PM";
    
    cell.timeLabel.text = [NSString stringWithFormat:@"%@:%@ %@・%@",
                           [dateDictionary objectForKey:@"ampm"],
                           [dateDictionary objectForKey:@"minute"],
                           AmPm,
                           [dateDictionary objectForKey:@"weekday"]];
    
    cell.countLabel.text = [newsDictionary objectForKey:JSON_KEY_NEWS_COUNT];
    
    return cell;
}

#pragma mark - Table view delegate

/**
 * セルの選択
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    TRACE(@"TABLE CEL SELECT : %d", (int)indexPath.row);
    NSArray *newsArray = [_model loadMonthList:indexPath.section];
    
    NewsDetailViewController *newsDetailView = [self.storyboard instantiateViewControllerWithIdentifier:@"newsDetail"];
    
    newsDetailView.getNewsData = [newsArray objectAtIndex:indexPath.row];
    
    [self.navigationController pushViewController:newsDetailView animated:YES];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
