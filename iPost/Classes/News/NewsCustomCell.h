//
//  NewsCustomCell.h
//  sPost.sponsor.mock.v1
//
//  Created by ヒロ企画 on 2014/06/17.
//  Copyright (c) 2014年 北川 義隆. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsCustomCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *listImage;

@property (weak, nonatomic) IBOutlet UILabel *yearLabel;

@property (weak, nonatomic) IBOutlet UILabel *dayLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;

@end
