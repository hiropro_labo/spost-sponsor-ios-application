//
//  NewsViewController.h
//  sPost.sponsor.mock.v1
//
//  Created by ヒロ企画 on 2014/06/17.
//  Copyright (c) 2014年 北川 義隆. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *pageTitleView;

@end
