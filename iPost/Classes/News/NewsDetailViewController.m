//
//  NewsDetailViewController.m
//  sPost.sponsor.mock.v1
//
//  Created by ヒロ企画 on 2014/06/17.
//  Copyright (c) 2014年 北川 義隆. All rights reserved.
//

#import "NewsDetailViewController.h"
#import "IPNewsModel.h"

@interface NewsDetailViewController ()
{
    NSMutableDictionary *newsData;
    NSMutableData *responsData;
}
@end

@implementation NewsDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    newsData = [self.getNewsData mutableCopy];
    //残ると嫌なので削除
    self.getNewsData = nil;
    
    //NavigationBar設定
    [IPUtility setNavigationBarSetting:self];
    [IPUtility setNavigationItem:self];
}

- (void)viewWillAppear:(BOOL)animated
{
    /// ニュース画像
    if(![newsData objectForKey:JSON_KEY_NEWS_FILE] || [newsData objectForKey:JSON_KEY_NEWS_FILE] == nil || [[newsData objectForKey:JSON_KEY_NEWS_FILE] isEqualToString:@""]){
        self.titleBar.frame = CGRectMake(0, 0
                                         , self.titleBar.frame.size.width, self.titleBar.frame.size.height);
        self.bodyLabel.frame = CGRectMake(0, self.titleBar.frame.size.height, self.bodyLabel.frame.size.width, self.bodyLabel.frame.size.height);
    }
    else{
        NSString *strURL = [NSString stringWithFormat:@"%@%@",
                        URL_IMG_NEWS_TOP,
                        [newsData objectForKey:JSON_KEY_NEWS_FILE]];
        NSURL *urlImage = [NSURL URLWithString:strURL];
        TRACE(@"urlImage = %@", urlImage);
        [self.topImageView setImageWithURL:urlImage placeholderImage:[UIImage imageNamed:IMAGE_PLACEHOLDERIMAGE_MENU_DETAIL]];
    }

    NSDictionary *dateDictionary = [IPUtility createDateTimeFromUnixTime:[newsData objectForKey:JSON_KEY_NEWS_UPDATED]];
    
    //年月日
    self.yearLabel.text = [NSString stringWithFormat:@"%@/%@", [dateDictionary objectForKey:@"year"],
        [dateDictionary objectForKey:@"month"]];
    
    //日付
    self.dayLabel.text = [NSString stringWithFormat:@"%02d",
                          [[dateDictionary objectForKey:@"day"] intValue]];
    
    //時間
    NSString *AmPm = @"AM";
    if ([[dateDictionary objectForKey:@"hour"] intValue] >= 12) AmPm = @"PM";
    
    self.timeLabel.text = [NSString stringWithFormat:@"%@:%@ %@・%@",
                           [dateDictionary objectForKey:@"ampm"],
                           [dateDictionary objectForKey:@"minute"],
                           AmPm,
                           [dateDictionary objectForKey:@"weekday"]];
    
    //タイトル
    self.titleLabel.text = [newsData objectForKey:JSON_KEY_NEWS_TITLE];
    
    //本文
    self.bodyLabel.text = [newsData objectForKey:JSON_KEY_NEWS_BODY];
    
    //いいねボタン
    [self.goodButton addTarget:self action:@selector(countUp) forControlEvents:UIControlEventTouchUpInside];
    if([[newsData objectForKey:JSON_KEY_NEWS_GOOD] intValue] == 1){
        self.goodButton.enabled = NO;
        [self.goodButton setImage:[UIImage imageNamed:@"btn_good_off.png"] forState:UIControlStateNormal];
    }
    
    //いいねカウント
    self.countLabel.text = [newsData objectForKey:JSON_KEY_NEWS_COUNT];
}

- (void)countUp
{
    NSLog(@"Good!!");
    [self.goodButton setImage:[UIImage imageNamed:@"btn_good_off.png"] forState:UIControlStateNormal];
    self.goodButton.enabled = NO;
    [self sendGoodCount:[newsData objectForKey:JSON_KEY_NEWS_ID]];
}

- (void)sendGoodCount:(NSString *)newsId
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:URL_API_SEND_GOOD]];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPMethod:@"POST"];
    
    NSString *token = [[NSUserDefaults standardUserDefaults] stringForKey:DEF_TOKEN];
    NSString *data = [NSString stringWithFormat:@"&token=%@&news_id=%@", token, newsId];
    
    [request setHTTPBody:[data dataUsingEncoding:NSUTF8StringEncoding]];
    
    TRACE(@"%@ : POST : %@", request, data);
    
    NSURLConnection *connection = [NSURLConnection connectionWithRequest:request delegate:self];
    if (connection) {
        TRACE(@"Good SEND : OK");
    } else {
        TRACE(@"Good SEND : ERROR");
    }
}

-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];

    CGFloat fixedWidth = self.bodyLabel.frame.size.width;
    CGSize newSize = [self.bodyLabel sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    CGRect newFrame = self.bodyLabel.frame;
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
    self.bodyLabel.frame = newFrame;
    
    CGFloat footerStartY = (self.bodyLabel.frame.origin.y + self.bodyLabel.frame.size.height);
    CGRect screenSize = [[UIScreen mainScreen] applicationFrame];
    CGFloat maxHeightSize = (screenSize.size.height - 44);
    
    if(maxHeightSize < footerStartY){
        self.goodButtonTitleView.frame = CGRectMake(0, footerStartY, self.goodButtonTitleView.frame.size.width, self.goodButtonTitleView.frame.size.height);
    }
    else{
        self.goodButtonTitleView.frame = CGRectMake(0, (maxHeightSize - self.goodButtonTitleView.frame.size.height), self.goodButtonTitleView.frame.size.width, self.goodButtonTitleView.frame.size.height);
    }
    //navigationBarとstatusBar分足してスクロールビューの高さ
    CGSize size = self.scrollView.contentSize;
    size.height = (self.bodyLabel.frame.origin.y + self.bodyLabel.frame.size.height) + 64 + self.goodButtonTitleView.frame.size.height;
    self.scrollView.contentSize = size;
}

- (void) connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    // データを初期化
    responsData = [[NSMutableData alloc] initWithData:0];

}

// [7]
- (void) connection:(NSURLConnection*)connection didReceiveData:(NSData *)data
{
    // データを追加する
    [responsData appendData:data];
}

// [8]
- (void) connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    // エラー処理
    // お好きにどうぞ！
    // --- 省略  ---
}

- (void) connectionDidFinishLoading:(NSURLConnection*)connection
{

    NSString *dataString = [[NSString alloc] initWithData:responsData encoding:NSUTF8StringEncoding];

    [newsData setObject:dataString forKey:JSON_KEY_NEWS_COUNT];
    self.countLabel.text = [newsData objectForKey:JSON_KEY_NEWS_COUNT];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
