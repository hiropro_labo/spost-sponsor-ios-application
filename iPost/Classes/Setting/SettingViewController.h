//
//  SettingViewController.h
//  iPost-Mock3
//
//  Created by 北川 義隆 on 2014/01/01.
//  Copyright (c) 2014年 北川 義隆. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingViewController : UITableViewController

- (NSString *)getUrl;

@end
