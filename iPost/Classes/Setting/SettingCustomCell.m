//
//  SettingCustomCell.m
//  iPost-Mock3
//
//  Created by 北川 義隆 on 2014/01/01.
//  Copyright (c) 2014年 北川 義隆. All rights reserved.
//

#import "SettingCustomCell.h"

@implementation SettingCustomCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setFrame:(CGRect)frame
{
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        CGRect switchFrame = _switchInput.frame;
        switchFrame.origin.x -= 10;
        switchFrame.origin.y += 1;
        _switchInput.frame = switchFrame;
    } else {
        int inset = 10;
        frame.origin.x += inset;
        frame.size.width -= 2 * inset;
    }
    
    [super setFrame:frame];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)putEffect
{
    // 角丸
    self.view.layer.cornerRadius = 3;
    
    // 枠線
    [self.view.layer setBorderColor:[[UIColor colorWithRed:0.7 green:0.7 blue:0.7 alpha:1] CGColor]];
    [self.view.layer setBorderWidth:1.0];
    
    // 影
    self.view.layer.shadowOpacity = 0.3;
    self.view.layer.shadowRadius = 1.0;
    self.view.layer.shadowOffset = CGSizeMake(0, 1);
    self.view.layer.shadowPath = [UIBezierPath bezierPathWithRect:self.view.bounds].CGPath;
    self.view.layer.shouldRasterize = YES;
    self.view.layer.rasterizationScale = [UIScreen mainScreen].scale;
    
    self.backgroundView = [[UIView alloc] initWithFrame:CGRectZero];
}
@end
