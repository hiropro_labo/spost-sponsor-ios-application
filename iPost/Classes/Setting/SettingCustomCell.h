//
//  SettingCustomCell.h
//  iPost-Mock3
//
//  Created by 北川 義隆 on 2014/01/01.
//  Copyright (c) 2014年 北川 義隆. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface SettingCustomCell : UITableViewCell

@property (nonatomic) IBOutlet UIView *view;
@property (nonatomic) IBOutlet UILabel *labelTitle;
@property (nonatomic) IBOutlet UISwitch *switchInput;

- (void)putEffect;

@end
