//
//  SettingDesign.m
//  iPost-Mock3
//
//  Created by 北川 義隆 on 2014/01/01.
//  Copyright (c) 2014年 北川 義隆. All rights reserved.
//

#import "SettingDesign.h"
#import "IPMainMenuModel.h"

@interface SettingDesign () {
    NSMutableArray *_arrayDesign;
    IPMainMenuModel *_model;
    
    NSString *email;
}
@end


@implementation SettingDesign

- (NSInteger)numberOfDesign
{
    return [_arrayDesign count];
}


- (void)makeDesign
{
    if ( ! _model || _model == nil) {
        _model = [[IPMainMenuModel alloc] init];
        [_model wait];
    }
    
    NSDictionary *_menuJson = [_model load:DEF_SLIDEMENU_JSON];
    NSArray *menuArray = [_menuJson objectForKey:JSON_KEY_SLIDEMENU_LIST];
    
    for (int i = 0; i < menuArray.count; i++) {
        NSDictionary *menuDictionary = [menuArray objectAtIndex:i];
        if([[menuDictionary objectForKey:JSON_KEY_SLIDEMENU_CONTROLLER] isEqualToString:@"email"]){
            email = [menuDictionary objectForKey:JSON_KEY_SLIDEMENU_EMAIL];
        }
    }
    
    for (int i = 0; i <= SETTING_DESIGN_ROWTYPE_APPQA; i++) {
        [self setDesign:[self makeDesignWithRowType:i]];
    }
}


/**
 * データ内容
 *  text      : タイトルテキスト
 *  switch    : switchの有無
 *  accessory : accessoryのタイプ
 *  selection : selectionStyle
 *  url       : 遷移先のurl文字列
 */
- (NSDictionary *)makeDesignWithRowType:(settingRowType)rowType
{
    NSString *text;
    BOOL bSwitch;
    UITableViewCellAccessoryType accessoryType;
    UITableViewCellSelectionStyle selectionStyle;
    NSString *strUrl;

    
    NSLog(@"SETTING_DESIGN_ROWTYPE_NOTIFICATION %ld", (long)SETTING_DESIGN_ROWTYPE_NOTIFICATION);
    NSLog(@"SETTING_DESIGN_ROWTYPE_EMAIL %ld", (long)SETTING_DESIGN_ROWTYPE_EMAIL);
    NSLog(@"SETTING_DESIGN_ROWTYPE_APPHOWTO %ld", (long)SETTING_DESIGN_ROWTYPE_APPHOWTO);
    NSLog(@"SETTING_DESIGN_ROWTYPE_APPQA %ld", (long)SETTING_DESIGN_ROWTYPE_APPQA);
    
    
    switch (rowType) {
        case SETTING_DESIGN_ROWTYPE_NOTIFICATION:
            text = [NSString stringWithFormat:@"アプリからのお知らせ"];
            bSwitch = TRUE;
            accessoryType = UITableViewCellAccessoryNone;
            selectionStyle = UITableViewCellSelectionStyleNone;
            strUrl = @"";
            break;

        case SETTING_DESIGN_ROWTYPE_EMAIL:
            text = @"メールでお問い合わせ";
            bSwitch = FALSE;
            accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            selectionStyle = UITableViewCellSelectionStyleBlue;
            strUrl = email ? email : HIROPRO_EMAIL;
            break;

        case SETTING_DESIGN_ROWTYPE_APPHOWTO:
            text = @"アプリの使い方";
            bSwitch = FALSE;
            accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            selectionStyle = UITableViewCellSelectionStyleBlue;
            strUrl = URL_HP_HOWTO;
            break;

        case SETTING_DESIGN_ROWTYPE_APPQA:
            text = @"よくある質問";
            bSwitch = FALSE;
            accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            selectionStyle = UITableViewCellSelectionStyleBlue;
            strUrl = URL_HP_FAQ;
            break;
            
        default:
            text = @"";
            bSwitch = FALSE;
            accessoryType = UITableViewCellAccessoryNone;
            selectionStyle = UITableViewCellSelectionStyleNone;
            strUrl = @"";
            break;
    }
    
    NSDictionary *dic = @{SETTING_DESIGN_TEXT : text,
                          SETTING_DESIGN_SWITCH : [NSNumber numberWithBool:bSwitch],
                          SETTING_DESIGN_ACCESSORY : [NSNumber numberWithInteger:accessoryType],
                          SETTING_DESIGN_SELECTIONSTYLE : [NSNumber numberWithInteger:selectionStyle],
                          SETTING_DESIGN_URL : strUrl
                          };
    
    return dic;
}

- (void)setDesign:(NSDictionary *)designData
{
    if (!_arrayDesign) {
        _arrayDesign = [[NSMutableArray alloc] init];
    }
    
    if (designData != nil) {
        [_arrayDesign addObject:designData];
    }
}

- (NSDictionary *)getDesignAtIndex:(NSInteger)index
{
    TRACE(@"index = %ld", (long)index);
    return _arrayDesign[index];
}

@end
