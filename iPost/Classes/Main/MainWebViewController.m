//
//  MainWebViewController.m
//  iPost.mock.v4
//
//  Created by ヒロ企画 on 2014/04/23.
//  Copyright (c) 2014年 北川 義隆. All rights reserved.
//

#import "MainWebViewController.h"
#import "SlideViewController.h"
#import "NewsViewController.h"
#import "NewsDetailViewController.h"
#import "IPNewsModel.h"

@interface MainWebViewController ()
{
    NSURLRequest *URLreq;
    NSDictionary *webViewData;
    
    SlideViewController *slideView;
    
    UIButton *buttonGoBack;
    UIButton *buttonGoFofward;
    
    UIBarButtonItem *backBarButtonItem;
    
    IPNewsModel *_model;
    NSDictionary *newsArraySection;
    
    //Navigaiton BackButton Image
    UIImage *image;
}
@end

@implementation MainWebViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.webView.delegate = self;
    
    //wevViewDataの取得
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    webViewData = [[NSDictionary alloc] init];
    webViewData = [ud objectForKey:@"webViewData"];
    
    //取得した後残ると嫌なのですぐに削除
    [ud removeObjectForKey:@"webViewData"];
    
    //wevViewDataを元にURLrequest
    NSString *webViewURL = [NSString stringWithFormat:@"%@%@/%@/", URL_WEBVIEW_ROOT, [webViewData objectForKey:@"controller"], CLIENT_ID];
    
    if([[webViewData objectForKey:@"controller"] isEqualToString:@"url"]){
        webViewURL = [webViewData objectForKey:@"url"];
    }
    
    NSLog(@"Display For %@", webViewURL);
    
    //Web表示
    NSURL *URL = [NSURL URLWithString:webViewURL];
    URLreq = [NSURLRequest requestWithURL:URL];
    [self.webView loadRequest: URLreq];
    
    //UIScrollViewを取得後、bouncesをNOに変更
    for (id subview in self.webView.subviews){
        if([[subview class] isSubclassOfClass: [UIScrollView class]]){
            ((UIScrollView *)subview).bounces = NO;
        }
    }
    
    [IPUtility setNavigationBarSetting:self];
    image = [IPUtility setNavigationItem:self];

    backBarButtonItem =
    [[UIBarButtonItem alloc] initWithImage:nil
                                     style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(pageBack)];
    
    self.navigationItem.leftBarButtonItem.enabled = NO;
    self.navigationItem.leftBarButtonItem = backBarButtonItem;
    
}

- (void)viewWillAppear:(BOOL)animated
{
    // News Model の読み込み
    if ( ! _model || _model == nil) {
        _model = [[IPNewsModel alloc] init];
    }
    
    [_model download:YES block:nil];
    
    newsArraySection = [[NSDictionary alloc] init];
    newsArraySection = [_model load:DEF_NEWS_JSON];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSString *requestString = [NSString stringWithFormat:@"%@", [request URL]];
    NSArray *requestSegment = [requestString componentsSeparatedByString:@"/"];

    
    if([[requestSegment objectAtIndex:3] isEqualToString:@"news"] && [[requestSegment objectAtIndex:4] isEqualToString:@"detail"]){
        
        NewsDetailViewController *newsDetail = [self.storyboard instantiateViewControllerWithIdentifier:@"newsDetail"];

        //ナビゲーションitem設定
        [IPUtility setNavigationItem:newsDetail];
        
        NSArray *keys = [newsArraySection allKeys];
        
        for (id key in keys) {
            NSArray *newsArray = [newsArraySection objectForKey:key];
            for (int i = 0; newsArray.count > i; i++) {

                if ([[newsArray[i] objectForKey:JSON_KEY_NEWS_ID] intValue] == [[requestSegment objectAtIndex:6] intValue]) {
                    newsDetail.getNewsData = newsArray[i];
                }
            }
        }
        
        
        [self.navigationController pushViewController:newsDetail animated:YES];
        
        return NO;
    }
    else if ([[requestSegment objectAtIndex:3] isEqualToString:@"news"]){
        
        NewsViewController *newsView = [self.storyboard instantiateViewControllerWithIdentifier:@"news"];
        
        [IPUtility setNavigationItem:newsView];
        
        [self.navigationController pushViewController:newsView animated:YES];
        
        return NO;
    }
    return YES;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    if(([[error domain]isEqual:NSURLErrorDomain]) && ([error code]!=NSURLErrorCancelled)) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"サーバーに接続できませんでした。\nインターネットに接続されているか確認して下さい。" delegate:self cancelButtonTitle:@"閉じる" otherButtonTitles:@"リトライ", nil];
        
        [alert show];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
            [self.webView stopLoading];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            break;
        case 1:
            [self.webView loadRequest:URLreq];
            break;
    }
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    if(self.webView.canGoBack == YES){
        
        backBarButtonItem =
        [[UIBarButtonItem alloc] initWithImage:image
                                         style:UIBarButtonItemStylePlain
                                        target:self
                                        action:@selector(pageBack)];
        self.navigationItem.leftBarButtonItem = backBarButtonItem;
        self.navigationItem.leftBarButtonItem.enabled = YES;
    }else{
        backBarButtonItem.image = nil;
        self.navigationItem.leftBarButtonItem.enabled = NO;
    }
    
}

/*
 scheme:native
 */

//webviewのスクロール無効
- (void)scrollDisable
{
    self.webView.scrollView.scrollEnabled = NO;
}

//次のページを表示する。
- (void)pageNext{
    [self.webView goForward];
}

//前のページを表示する。
- (void)pageBack
{
    [self.webView goBack];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
