//
//  MainWebViewController.h
//  iPost.mock.v4
//
//  Created by ヒロ企画 on 2014/04/23.
//  Copyright (c) 2014年 北川 義隆. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainWebViewController : UIViewController <UIWebViewDelegate>

@property (strong, nonatomic) IBOutlet UIWebView *webView;

@end
