//
//  ViewController.m
//  iPost-Mock3
//
//  Created by 北川 義隆 on 2013/10/26.
//  Copyright (c) 2013年 北川 義隆. All rights reserved.
//

#import "ViewController.h"
#import "SlideViewController.h"


@interface ViewController ()
{
    IPMainMenuModel *_mainMenuModel;
    GLobalModel   *_globaleModel;
    IPNewsModel     *_newsModel;
    IPSettingModel  *_settingModel;
    
    NSDictionary *globalData;
}
@end


@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupModels];
    [self showUsingBlocks];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    TRACE(@"");
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    TRACE(@"");
}

- (void)setupModels
{
    if (!_globaleModel || _globaleModel == nil) _globaleModel = [[GLobalModel alloc] init];
    if (!_mainMenuModel || _mainMenuModel == nil) _mainMenuModel = [[IPMainMenuModel alloc] init];
    if (!_newsModel || _newsModel == nil) _newsModel = [[IPNewsModel alloc] init];
    if (!_settingModel || _settingModel == nil) _settingModel = [[IPSettingModel alloc] init];
}

/**
 * JSONダウンロード
 */
- (void)showUsingBlocks
{
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:hud];
    hud.labelText = @"Loading...";
    
    //[self teamLogoAnimation];
    
    [hud showAnimated:YES whileExecutingBlock:^{
        
        // メンテJSONロード
        [_globaleModel download:YES block:nil];
        
        // メインJSONロード
        [_mainMenuModel download:YES block:nil];
        
        // ニュースJSONロード
        [_newsModel download:YES block:nil];
        
        // セッティングJSONロード
        //[_settingModel download:YES block:nil];
        
    } completionBlock:^{
        
        BOOL failed = NO;
        
        for (int i = 0; i < 5; i++) {
            [NSThread sleepForTimeInterval:0.2f];
            if([_mainMenuModel saveIsOk] == YES &&
               [_globaleModel saveIsOk] == YES &&
               [_newsModel saveIsOk] == YES){
                break;
            }
            else{
                failed = YES;
            }
        }
        
        if(failed == NO){
            [hud removeFromSuperview];
            globalData = [_globaleModel load:DEF_GLOBAL_JSON];
            if ([[globalData objectForKey:JSON_KEY_GLOBAL_ALIVE] intValue] == 0) {
                [self locationMain];
            }
            else if([[globalData objectForKey:JSON_KEY_GLOBAL_ALIVE] intValue] == 1){
                [IPUtility mainteAlertShow];
            }
            else if([[globalData objectForKey:JSON_KEY_GLOBAL_ALIVE] intValue] == 2){
                [IPUtility stopPublishing];
            }
            else{
                [IPUtility convertAlertShow];
            }
        }
        else{
            [self makeAlertView:1];
        }

    }];
}

- (void)makeAlertView:(int)tag
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message:@"サーバーに接続できませんでした。\nインターネットに接続されているか確認して下さい。"
                                                   delegate:self
                                          cancelButtonTitle:@"閉じる"
                                          otherButtonTitles:@"リトライ", nil];
    alert.tag = tag;
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
            break;
        case 1:
            [self showUsingBlocks];
        break;
    }

}

/**
 * Meinte to Main
 */
- (void)locationMain
{
    SlideViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SlideViewController"];
    [self presentViewController:controller animated:NO completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
