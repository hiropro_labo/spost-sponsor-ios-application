//
//  ReportViewController.h
//  iPost.mock.v3
//
//  Created by 北川 義隆 on 2014/04/15.
//  Copyright (c) 2014年 北川 義隆. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReportViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIView *tablebgView;
@property (strong, nonatomic) IBOutlet UIView *btnbgView;
@property (strong, nonatomic) IBOutlet UIView *viewSeparator;
@property (strong, nonatomic) IBOutlet UIView *viewSeparator2;

@property (strong, nonatomic) IBOutlet UITextField *fieldReason;
@end
