//
//  SlideViewController.m
//  iPost.mock.v4
//
//  Created by ヒロ企画 on 2014/04/22.
//  Copyright (c) 2014年 北川 義隆. All rights reserved.
//

#import "SlideViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "NavigationController.h"
#import "IPMainMenuModel.h"
#import "SlideMenuCustomCell.h"

#define MENU_CUSTOMCELL @"SlideMenuCustomCell"

@interface SlideViewController ()
{
    UITableViewController *menuTableViewController;
    IPMainMenuModel   *_model;
    NSDictionary *_menuJson;
    NSMutableArray *menuArray;
    NSURL *_url;
    
    NSString *phoneNumber;
    NSString *emailAddress;
}

@property (nonatomic, assign) BOOL isOpen;
@property (nonatomic, retain) IBOutlet UIView *dummyView;

@end

@implementation SlideViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // MainMenu Model の読み込み
    if ( ! _model || _model == nil) {
        _model = [[IPMainMenuModel alloc] init];
        [_model wait];
    }
    
    [self jsonLoad];
    
    [self setUpSlideMenu];
    
    //navigationController
    UINavigationController *navigationController = [[NavigationController alloc] init];
    [self addChildViewController:navigationController];
    [navigationController didMoveToParentViewController:self];
    [self.view addSubview:navigationController.view];
    
    navigationController.view.layer.shadowOpacity = 0.3;
    navigationController.view.layer.shadowOffset = CGSizeMake(-10.0, 10.0);
    navigationController.view.layer.cornerRadius = 10;
    
    [self setWebViewData:0];

    UIViewController *viewController1 = [self.storyboard instantiateViewControllerWithIdentifier:@"webView"];
    [self setFirstViewController:viewController1];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [menuTableViewController.tableView registerNib:[UINib nibWithNibName:@"SlideMenuCustomCell" bundle:nil] forCellReuseIdentifier:MENU_CUSTOMCELL];
}

- (void)setUpSlideMenu
{
    //スライドメニューの設定
    menuTableViewController = [[UITableViewController alloc] init];
    menuTableViewController.tableView.delegate = self;
    menuTableViewController.tableView.dataSource = self;
    
    [menuTableViewController.view setFrame:_dummyView.frame];
    [self addChildViewController:menuTableViewController];
    [menuTableViewController didMoveToParentViewController:self];
    [self.view addSubview:menuTableViewController.view];
    menuTableViewController.tableView.backgroundColor = [UIColor blackColor];
    menuTableViewController.tableView.separatorColor = [UIColor clearColor];
}

#pragma mark - setFirstViewController
-(void)setFirstViewController:(UIViewController *)viewController
{
    //ナビゲーションバーの設定
    UIImage *image = [IPUtility setNavigationBarSetting:viewController];

    UIBarButtonItem *rightBarItem =
    [[UIBarButtonItem alloc] initWithImage:image
                                     style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(slide:)];
    [viewController.navigationItem setRightBarButtonItem:rightBarItem];
    
    UINavigationController *navigationController = [self.childViewControllers lastObject];
    [navigationController setViewControllers:[NSArray arrayWithObjects:viewController, nil]];
}

-(void)removeFirstViewController
{
    UINavigationController *navigationController = [self.childViewControllers lastObject];
    NSMutableArray *viewControllers = [NSMutableArray arrayWithArray:navigationController.viewControllers];
    [viewControllers removeAllObjects];
    navigationController.viewControllers = viewControllers;
}

- (void)jsonLoad
{
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:hud];
    hud.labelText = @"Loading...";
    
    [hud showAnimated:YES whileExecutingBlock:^{
        _menuJson = [_model load:DEF_SLIDEMENU_JSON];
        _menuJson = [_menuJson objectForKey:JSON_KEY_SLIDEMENU_LIST];
        
        menuArray = [[NSMutableArray alloc] init];
        
        for (NSDictionary *obj in _menuJson) {
            if (obj != nil)
            {
                [menuArray addObject:obj];
            }
        }
        
        NSSortDescriptor *sortDispNo = [[NSSortDescriptor alloc] initWithKey:@"position" ascending:YES];
        NSArray *sortDescArray = [NSArray arrayWithObjects:sortDispNo, nil];
        [menuArray sortUsingDescriptors:sortDescArray];
        
        [menuTableViewController.tableView reloadData];
        
    } completionBlock:^{
        [hud removeFromSuperview];
    }];
    
    [_model wait];

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [menuArray count];
}

/**
 * テーブル全体のセクションの数を返す
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    SlideMenuCustomCell *cell = [menuTableViewController.tableView dequeueReusableCellWithIdentifier:MENU_CUSTOMCELL forIndexPath:indexPath];
    
    // Configure the cell...
    cell.textLabel.text = menuArray[indexPath.row][JSON_KEY_SLIDEMENU_NAME];
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.font = [UIFont boldSystemFontOfSize:14];
    
    // 画像ダウンロード    
    NSString *strURL = [NSString stringWithFormat:@"%@%@",
                        URL_IMG_SLIDEMAIN_MENU,
                        menuArray[indexPath.row][JSON_KEY_SLIDEMENU_FILENAME]];
    
    //NSURL *urlImage = [NSURL URLWithString:strURL];
    
    NSURL *iconImageUrl = [NSURL URLWithString:strURL];
    
    NSData *iconImageData = [[NSData alloc] initWithContentsOfURL:iconImageUrl];
    
    UIImage *iconImage = [UIImage imageWithData:iconImageData];
    
    
    UIGraphicsBeginImageContext(CGSizeMake(74, 74));
    [iconImage drawInRect:CGRectMake(20, 20, 34, 34)];
    iconImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    
    
    [cell.imageView setImage:iconImage];
//    [cell.imageView setImageWithURL:urlImage placeholderImage:[UIImage imageNamed:IMAGE_PLACEHOLDERIMAGE_MENU2]];
    
    cell.backgroundColor = [UIColor clearColor];
    cell.imageView.backgroundColor = [UIColor clearColor];
    
    if(indexPath.row % 2 == 0){
        cell.backgroundColor = RGB(38, 38, 38);
        cell.imageView.backgroundColor = RGB(38, 38, 38);
    }
    
    return cell;
}

#pragma mark - UITableViewControllerDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    if([menuArray[indexPath.row][@"viewtype"] isEqualToString:@"1"] ||
       [menuArray[indexPath.row][@"controller"] isEqualToString:@"news"] ||
       [menuArray[indexPath.row][@"controller"] isEqualToString:@"setting"])
    {
        [self removeFirstViewController];
        NSString *viewName = [NSString stringWithFormat:@"%@", menuArray[indexPath.row][@"controller"]];
        [self setFirstViewController:[self.storyboard instantiateViewControllerWithIdentifier:viewName]];
    }
    else if ([menuArray[indexPath.row][@"controller"] isEqualToString:@"tel"]){
        phoneNumber = [NSString stringWithFormat:@"%@", menuArray[indexPath.row][@"tel"]];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"通話" message:@"通話発信します\nよろしいですか？" delegate:self cancelButtonTitle:@"いいえ" otherButtonTitles:@"はい", nil];
        alert.tag = 1;
        [alert show];
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
    else if([menuArray[indexPath.row][@"controller"] isEqualToString:@"email"]){
        emailAddress = [NSString stringWithFormat:@"%@", menuArray[indexPath.row][@"email"]];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"メール送信" message:@"メーラーを立ち上げます\nよろしいですか？" delegate:self cancelButtonTitle:@"いいえ" otherButtonTitles:@"はい", nil];
        alert.tag = 2;
        [alert show];
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
    else{
        [self removeFirstViewController];
        [self setWebViewData:(int)indexPath.row];
        [self setFirstViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"webView"]];
    }
    
    [self slide:nil];
}

#pragma mark - slide
-(void)slide:(id)sender
{
    _isOpen = !_isOpen;
    
    if(_isOpen == YES) [menuTableViewController.tableView reloadData];
    
    UINavigationController *firstNavigationController = [self.childViewControllers objectAtIndex:1];
    UIView *firstView = firstNavigationController.view;
    [UIView animateWithDuration:0.3 animations:^{
        CGFloat originX = _isOpen ? -200 : 0;
        CGRect frame = firstView.frame;
        frame.origin.x = originX;
        firstView.frame = frame;
    } completion:^(BOOL finished){
        
    }];
}

- (void)setWebViewData:(int)index
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:menuArray[index] forKey:@"webViewData"];
    [ud synchronize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1) {
        switch (buttonIndex) {
            case 0:
                break;
            case 1:
                [IPUtility telephoneCallWithStringNumber:phoneNumber];
                break;
        }
    }
    if (alertView.tag == 2) {
        
        switch (buttonIndex) {
            case 0:
                break;
            case 1:
                [IPUtility mailToWithStringAddress:emailAddress];
                break;
        }
    }
    else{
        
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
