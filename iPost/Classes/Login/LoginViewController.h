//
//  LoginViewController.h
//  iPost.mock.v3
//
//  Created by 北川 義隆 on 2014/03/20.
//  Copyright (c) 2014年 北川 義隆. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController <UITextFieldDelegate>

@end
