//
//  LoginViewController.m
//  iPost.mock.v3
//
//  Created by 北川 義隆 on 2014/03/20.
//  Copyright (c) 2014年 北川 義隆. All rights reserved.
//

#import "LoginViewController.h"
#import "ViewController.h"

@interface LoginViewController () {
    IBOutlet UITextField *_fieldLogin;
    IBOutlet UITextField *_fieldSecret;
    
    UIAlertView  *_alert;
    NSString *_login;
}

@end

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    TRACE(@"");
    [_fieldLogin becomeFirstResponder];
    _fieldLogin.delegate = self;
    _fieldSecret.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    TRACE(@"");
    
    _fieldLogin.text = [[NSUserDefaults standardUserDefaults] stringForKey:DEF_LOGIN_ID];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - private method
// 入力項目チェック
- (void)checkField:(NSString *)login secret:(NSString *)secret
{
    if ([login isEqualToString:@""] || [secret isEqualToString:@""]) {
        _alert = [[UIAlertView alloc]
                  initWithTitle:@"注意！"
                  message:@"ログインID、パスワードが未入力です。"
                  delegate:self
                  cancelButtonTitle:@"閉じる"
                  otherButtonTitles:nil, nil];
        
        [_alert show];
        
        if ([_fieldLogin.text isEqualToString:@""]) {
            [_fieldLogin becomeFirstResponder];
        }
    } else {
        [self sendLogin:login secret:secret];
    }
}

// 送信処理
-(void)sendLogin:(NSString *)login secret:(NSString *)secret
{
    [[NSUserDefaults standardUserDefaults] setObject:login forKey:DEF_LOGIN_ID];
    
    NSDictionary *params = @{@"login_id" : login,
                             @"secret"   : secret};
    
    NSURL *apiURL = [NSURL URLWithString:URL_API_LOGIN];
    AFHTTPClient *client = [[AFHTTPClient alloc] initWithBaseURL:apiURL];
    
    [client postPath:nil
          parameters:params
             success:^(AFHTTPRequestOperation *operation, id data) {
                 TRACE(@"success");
                 
                 [[NSUserDefaults standardUserDefaults] setObject:operation.responseString forKey:DEF_LOGIN_JSON];
                 [self locationMain];
                 
             }
             failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                 TRACE(@"failure");
                 
                 TRACE(@"Error : %@", error);
                 _alert = [[UIAlertView alloc]
                           initWithTitle:nil
                           message:@"ログインに失敗しました。"
                           delegate:self
                           cancelButtonTitle:@"閉じる"
                           otherButtonTitles:nil, nil];
                 [_alert show];
             }];
}


#pragma mark - UITextField delegate method
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    switch (textField.tag) {
        case 1:
            [_fieldSecret becomeFirstResponder];
            break;
        case 2:
            [self checkField:_fieldLogin.text secret:_fieldSecret.text];
            break;
    };
    return YES;
}


#pragma mark - location method

/**
 * Meinte to Main
 */
- (void)locationMain
{
    ViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"mainte"];
    [self presentViewController:controller animated:NO completion:nil];
}
@end
