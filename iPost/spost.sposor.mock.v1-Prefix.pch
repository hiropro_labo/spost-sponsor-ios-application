//
//  Prefix header
//
//  The contents of this file are implicitly included at the beginning of every source file.
//

#import <Availability.h>

#ifndef __IPHONE_5_0
#warning "This project uses features only available in iOS SDK 5.0 and later."
#endif

#ifdef __OBJC__
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
//#import <MapKit/MapKit.h>
#import <AddressBook/AddressBook.h>
#import "IPHTTPLoader.h"
#import "IPUtility.h"
#import "AFNetworking.h"
#import "OHAttributedLabel.h"
#import "MBProgressHUD.h"
#endif


// クライアントID
#define CLIENT_ID         @"6"

// パラメータ
#define DEVICE            @"ios"

/// URL定義
// 本番用
#define URL_API_ROOT        @"http://api.spost.jp/"
#define URL_IMG             @"http://img.spost.jp/"
#define URL_WEBVIEW_ROOT    @"http://app.spost.jp/"

// デバッグ用
//#define URL_API_ROOT        @"http://api.hirolabo.net/"
//#define URL_IMG             @"http://admin.hirolabo.net/assets/img/app/"

// ファイル名
#define API_FILE_MAIN        @"top/"
#define API_FILE_NEWS        @"news/"
#define API_FILE_SETTING     @"setting/"
#define API_FILE_MAINTE      @"global/"
#define API_FILE_SLIDEMENU   @"switcher/"
#define API_FILE_GOOD        @"news/cnt/"

#define API_FILE_TOKEN       @"token/"
#define API_FILE_LOGIN       @"login/"

#define API_FILE_NOTIF       @"notification/"

// API URL
#define URL_API_NEWS        URL_API_ROOT API_FILE_NEWS CLIENT_ID         @"/"
#define URL_API_SLIDEMENU   URL_API_ROOT API_FILE_SLIDEMENU CLIENT_ID    @"/"
#define URL_API_SEND_GOOD   URL_API_ROOT API_FILE_GOOD CLIENT_ID @"/"

#define URL_API_MAINTE      URL_API_ROOT API_FILE_MAINTE CLIENT_ID   @"/"
#define URL_API_MAIN        URL_API_ROOT API_FILE_MAIN CLIENT_ID     @"/"
#define URL_API_SETTING     URL_API_ROOT API_FILE_SETTING CLIENT_ID  @"/"

#define URL_API_TOKEN       URL_API_ROOT API_FILE_TOKEN
#define URL_API_LOGIN       URL_API_ROOT API_FILE_LOGIN

#define URL_API_NOTIF       URL_API_ROOT API_FILE_SETTING API_FILE_NOTIF


// 他 URL
#define IPOST_URL           @"http://i-post.jp/"
#define URL_HP_ROOT         @"http://hiropro.co.jp/"
#define URL_HP_COMPANY      URL_HP_ROOT @"#about"

#define URL_HP_FAQ          @"http://hiropro.sakura.ne.jp/ipost/faq.html"
#define URL_HP_HOWTO        @"http://hiropro.sakura.ne.jp/ipost/howto.html"
#define HIROPRO_EMAIL       @"support@hiropro.co.jp"

// 画像ファイルの取得先
#define DIR_IMG_TOP              @"top/"
#define DIR_IMG_NEWS             @"app/news/"
#define DIR_IMG_SLIDEMENU_ICON   @"assets/img/common/icon/"
#define DIR_IMG_ICON             @"app/icon/"

// 画像ファイルのURL
#define URL_IMG_NEWS             URL_IMG DIR_IMG_NEWS CLIENT_ID @"/"
#define URL_IMG_NEWS_TOP         URL_IMG DIR_IMG_NEWS CLIENT_ID @"/"

#define URL_IMG_SLIDEMAIN_MENU   URL_WEBVIEW_ROOT DIR_IMG_SLIDEMENU_ICON
#define URL_IMG_SHOP             URL_IMG          DIR_IMG_SHOP            CLIENT_ID @"/"
#define URL_IMG_MAIN             URL_IMG          DIR_IMG_TOP             CLIENT_ID @"/"
#define URL_IMG_COUPON           URL_IMG          DIR_IMG_COUPON          CLIENT_ID @"/"
#define URL_IMG_MENU_TOP         URL_IMG          DIR_IMG_MENU_TOP        CLIENT_ID @"/"
#define URL_IMG_MENU_CATE        URL_IMG          DIR_IMG_MENU_CATE       CLIENT_ID @"/"
#define URL_IMG_MENU_ITEM        URL_IMG          DIR_IMG_MENU_ITEM       CLIENT_ID @"/"
#define URL_IMG_ICON             URL_WEBVIEW_ROOT DIR_IMG_SLIDEMENU_ICON
#define URL_IMG_LOGO             URL_IMG          DIR_IMG_ICON            CLIENT_ID @"/"

// NO IMAGE
#define IMAGE_PALCEHOLDERIMAGE_NEWS2        @"blank160x160.png"
#define IMAGE_PLACEHOLDERIMAGE_MAIN         @"blank300x190.jpg"
#define IMAGE_PLACEHOLDERIMAGE_MAIN_SHOP    @"blank300x210.jpg"
#define IMAGE_PLACEHOLDERIMAGE_COUPON       @"blank280x300.jpg"
#define IMAGE_PLACEHOLDERIMAGE_NEWS         @"blank280x188.jpg"
#define IMAGE_PLACEHOLDERIMAGE_MENU1        @"blank320x175.jpg"
#define IMAGE_PLACEHOLDERIMAGE_MENU2        @"blank74x74.jpg"
#define IMAGE_PLACEHOLDERIMAGE_MENU_DETAIL  @"blank300x170.jpg"


// NSUserDefaults の名前を指定
#define DEF_GLOBAL_JSON         @"globalJSON"
#define DEF_SLIDEMENU_JSON      @"slideMenuJSON"
#define DEF_NEWS_JSON           @"newsJSON"
#define DEF_SETTING_JSON        @"settingJSON"
#define DEF_TOKEN               @"token"
#define DEF_LOGIN_JSON          @"loginJSON"
#define DEF_LOGIN_ID            @"login_id"

#define DEF_TIME_NAME          @"TIME"

#define DEF_MAINTE_JSON_TIME       DEF_MAINTE_JSON DEF_TIME_NAME
#define DEF_SLIDEMENU_JSON_TIME    DEF_SLIDEMENU_JSON DEF_TIME_NAME
#define DEF_NEWS_JSON_TIME         DEF_NEWS_JSON DEF_TIME_NAME
#define DEF_SETTING_JSON_TIME      DEF_SETTING_JSON DEF_TIME_NAME
#define DEF_MAIN_IMG_TIME      @"mainImg" DEF_TIME_NAME

// 開発用
#ifdef DEBUG
#define TRACE(fmt, ...) NSLog((@"%s(%d) " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#define TRACE(...);
#endif

// 色指定 10進数で出来るのを簡略化
#define RGB(r, g, b) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1]
#define RGBA(r, g, b, a) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:(a)]

// 色
#define COLOR_BASS       RGB(183,56,44)
#define COLOR_MAIN_BG    [UIColor colorWithRed:.9 green:.9 blue:.9 alpha:1]
#define COLOR_TICKER     RGB(46,46,46)

#ifdef __IPHONE_6_0
# define LINE_BREAK_WORD_WRAP NSLineBreakByCharWrapping
#else
# define LINE_BREAK_WORD_WRAP UILineBreakModeCharacterWrap
#endif

