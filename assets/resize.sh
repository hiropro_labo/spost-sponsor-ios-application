#!/usr/bin/env bash

convert $1 -geometry 29x29 29pt.png
convert $1 -geometry 58x58 29pt@2x.png

convert $1 -geometry 40x40 40pt.png
convert $1 -geometry 80x80 40pt@2x.png

convert $1 -geometry 50x50 50pt.png
convert $1 -geometry 100x100 50pt@2x.png

convert $1 -geometry 57x57 57pt.png
convert $1 -geometry 114x114 57pt@2x.png

convert $1 -geometry 120x120 60@2x.png

convert $1 -geometry 72x72 72pt.png
convert $1 -geometry 144x144 72pt@2x.png

convert $1 -geometry 76x76 76pt.png
convert $1 -geometry 152x152 76pt@2x.png
